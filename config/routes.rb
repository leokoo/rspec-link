Rails.application.routes.draw do
  resources :links, only: [:show, :new, :create, :edit] do
    resource :upvote, only: [:create]
    resource :downvote, only: [:create]
  end



  get "/api/v1/links", to: "links#index" , as: "json_index"

  post "/api/v1/links", to: "links#create" , as: "json_create"
  get "/new", to: "new_links#index", as: "new_links"
  get "/change_vote", to: "links#change_vote", as: "change_vote"
  root to: "links#index"
end
