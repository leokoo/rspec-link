class LinksController < ApplicationController
  
  def index	

    respond_to do |format|
      
      format.json do
        byebug
      end

      format.html do
        @link = Link.all
      end

    end

  end
  
  def new
  	
  end

  def create
    # byebug
  	@link = Link.new(link_params)
  		if @link.save
  			flash[:message] = "Successful"
        LinkMailer.new_link(@link)
  			redirect_to root_path
  		else
  			render :new
  		end
  end

  def show
    # byebug
    @link = Link.find(params[:id])
    # render :show
  end

  def change_vote
    # @params_file = params
    @link = Link.find(params[:link][:id])

    if params[:commit] == "Downvote"
      @link.downvote
      # @link.save
      # byebug
    elsif params[:commit] == "Upvote"
      @link.upvote
      # @link.save
      # byebug
    end
    # render :index
    redirect_to @link
  end

	private

	def link_params
		params.require(:link).permit(:title, :url, :image)
	end
end
